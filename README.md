# MiniPlex - Movie Ticket Booking

## Description
MiniPlex is a movie ticket booking application built using Tkinter and MySQL. It provides users with a platform to book movie tickets, register new accounts, and login securely. Users can also view available movies, select show timings, and make payments for their bookings.

## Features
- User-friendly GUI built with Tkinter.
- Secure login system with user registration.
- Movie selection with date and time options.
- Ticket booking with payment integration.
- Integration with MySQL for data storage.

## Installation
To run the MiniPlex Movie Ticket Booking application locally, follow these steps:
1. Clone the repository: `git clone https://github.com/your-username/miniplex.git`
2. Navigate to the project directory: `cd miniplex`
3. Install dependencies: `pip install -r requirements.txt`
4. Set up MySQL database:
   - Create a MySQL database named 'demo'.
   - Import the provided database schema from `database_schema.sql`.
5. Update MySQL connection details in the code (`main.py`) with your database credentials.
6. Start the application: `python main.py`

## Usage
To use the MiniPlex Movie Ticket Booking application:
1. Open the application.
2. Log in with your credentials or sign up for a new account.
3. Select a movie, date, time, and number of tickets.
4. Enter payment details and confirm your booking.
5. View booking details and enjoy your movie!

## Support
If you encounter any issues or have any questions, please feel free to [open an issue](https://github.com/your-username/miniplex/issues). We're here to assist you!

## Contributing
Contributions are welcome! If you'd like to contribute to the project, please follow these steps:
1. Fork the repository.
2. Create a new branch: `git checkout -b feature/new-feature`
3. Make your changes and commit them: `git commit -am 'Add new feature'`
4. Push to the branch: `git push origin feature/new-feature`
5. Submit a pull request.

## Authors
- [KeerthiNune](nunekeerthi05@gmail.com)
- [SathwikaPutrevu](sathwikaputrevu5@gmail.com)
- [VyshnaviKambhampati](vyshnavikambhampati12@gmail.com)

## License
This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Project Status
The MiniPlex Movie Ticket Booking application is functional and actively maintained. We're continuously working on adding new features and improvements.
